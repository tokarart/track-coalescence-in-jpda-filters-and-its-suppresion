## Code repository for thesis "Track coalescence in JPDA filters and its suppresion"
By Bc. Artem tokarevskikh under the supervision of doc. Ing. Kamil Dedecius, Ph.D., FIT CVUT.

### Evironment setup
Requires anaconda installation.
```bash
git clone ... thesis_tokarart
cd thesis_tokarart
conda env create -f env/environment.yml -n thesis_tokarart
conda activate thesis_tokarart
cd env/gospapy
pip install -e .
```

### Trajectory export
We used Blender 4.1. Open the file `source/utils/traj.blend`, select the NURBS curve and run the script `source/utils/point_export_script.py`. Fill the export path in advance. Exported and prepared trajectories for experiments are placed in `source/data`.

### Sources
Directory `source` contains two scripts which run the experiments
- `source/experiment_baseline_pruning.py`
- `source/experiment_ours.py`.

Results are saved to `source/experiment_results` and then analyzed in notebooks 
- `source/analyze_results.ipynb`
- `source/plots.ipynb`

Second notebok prepares the figures used in the thesis.
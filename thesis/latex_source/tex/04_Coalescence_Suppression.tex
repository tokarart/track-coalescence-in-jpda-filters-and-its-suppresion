\chapter{Coalescence suppression}
\label{ch:coalescence_suppression}

As established in previous chapters, track coalescence presents a significant challenge for multi-target tracking systems, particularly those utilizing JPDA filters. This chapter delves into the various techniques developed to combat this phenomenon, reviewing their strengths and limitations. We will then present a novel approach that combines adaptive validation gating with strategic hypothesis pruning, offering a robust and computationally efficient solution for maintaining track integrity in the presence of closely spaced targets.
\begin{figure}
    \centering
    \includesvg[width=.7\textwidth]{figures/04_02_coalescence_example.svg}
    \caption{Track coalescence example.}
    \label{fig:coalescence_example_04}
\end{figure}

\section{Exact nearest neighbor}\label{sec:ENNPDA}
One of the two main paradigms in tackling the coalescence problem is hypothesis pruning. The exact nearest-neighbor PDA (ENNPDA) presented in \cite{ennpda_fitzgerald} is a very simple yet powerful approach. Originally, it was designed to run in real time on a microprocessor, so the original PDA was optimized, and pruning was introduced both to address the coalescence problem and to lower the computational cost. 

Basically, once the probabilities $\beta_t^{i,j}$ from Equation \eqref{eq:JPDA_marginal_assoc_probs} are calculated, ENNPDA employs a nearest neighbor approach. This means that each measurement is assigned to the track with the highest association probability, creating a one-to-one correspondence. Unlike standard nearest-neighbor methods that solely rely on the distance between a track's predicted position and a measurement, ENNPDA factors in the association probability. This leads to more accurate associations, especially in situations where tracks and measurements are close to each other. 

Let us take a closer look at the set of hypotheses $\Theta_t$. We will use the notation of \cite{Blom_coalscence_avoiding}. We first define a target detection vector $\phi_t \in \{ 0,1 \}^n$ as follows
\begin{align}
    \phi_{j,t}(\Theta^i_t) =
    \begin{cases}
        1 & \text{if the $j$th target was detected in hypothesis $\Theta^i_t$}, \\
        0 & \text{otherwise.}
    \end{cases}
\end{align}
Next, we define the measurement association vector $\psi_t \in \{ 0,1 \}^{m_t}$ as
\begin{align}
    \psi_{j,t}(\Theta^i_t) =
    \begin{cases}
        1 & \text{if the $j$-th measurement belongs to detected target} \\
        0 & \text{otherwise.}
    \end{cases}
\end{align}
We also define the operator $D(x) = \sum_{i} x_i$. Then $D(\phi_t(\Theta^i_t))$ stands for the number of detected targets, and $D(\psi_{t}(\Theta^i_t))$ stands for the number of measurements associated with the detected tracks. 

Then we define the sequence of permutation matrices $\{ \chi_t \}$, which randomly permutes the measurements to match them with the detected targets. We will not delve into the mathematical details of this permutation. 

Using this notation in \cite{Blom_coalscence_avoiding} we split the series of association hypotheses $\{ \Theta_t \}$ into two subsets: $(\phi, \psi)$-hypotheses and $(\phi, \chi)$-hypotheses. $(\phi, \psi)$-hypothesis is about the classification of measurements as target-originated or clutter, while the $(\phi, \chi)$-hypothesis is about the ordering of measurements to targets.

% That is, for every $(\phi, \chi)$-hypothesis $\Theta^i_t$, $D(\psi_{t}(\Theta^i_t)) = D(\phi_t(\Theta^i_t))$, since the number of detected tracks and the number of measurements associated with the tracks must be equal to satisfy the assumptions presented in Section \ref{sec:JPDA}.

In ENNPDA, we take all feasible association hypotheses $\{ \Theta_t \}$ that satisfy $D(\psi_{t}(\Theta^i_t)) = D(\phi_t(\Theta^i_t)) \leq \min\{ n, m_t \}$ and select one that
\begin{align}
    \hat{\Theta}_t = \arg \max_{\Theta_t} = Pr \{ \Theta_t | \mathbf{Z}_{1:t} \}.
\end{align}
That is, we prune all $(\phi, \chi)$-hypotheses except for one $\hat{\Theta}_t$ with the highest likelihood and use it for update. The resulting ENNPDA appeared to be remarkably insensitive to track coalescence in case there is no clutter and no missed detections. However, the dramatic pruning used for ENNPDA leads to an undesirable sensitivity to clutter and missed detections \cite{rong_li_nn_tracking_in_clutter}. As a result, by using the ENNPDA coalescence suppression method, we lose the robust tracking performance of JPDA, which is undesirable. Nevertheless, the ENNPDA approach pioneered the pruning strategy to mitigate coalescence and laid the foundation for the development of more advanced pruning techniques.

\section{JPDA*}\label{sec:JPDA_star}
In order to benefit from pruning while maintaining the insensitivity of JPDA to clutter and missed detections, the JPDA* approach was introduced \cite{Blom_coalscence_avoiding}. It was found that track coalescence could be avoided by pruning $\chi$-hypotheses. In particular, we prune the permutations of the hypothesis with the highest likelihood in order to reduce the uncertainty for resulting posterior.

In order to keep the filter robust to clutter and misdetections, we must not prune any $\phi$ or $\psi$ hypotheses \cite{Blom_coalscence_avoiding}. The combination of these two findings applied to JPDA is called JPDA*. 

In JPDA*, we first evaluate all feasible $(\phi_t, \psi_t)$-hypotheses, then per $(\phi_t, \psi_t)$-hypothesis we prune all less likely $\chi$-hypotheses, that is, permutations of the hypothesis with the highest likelihood. Mathematically, for every $\phi_t$ and $\psi_t$, satisfying $D(\phi_t) = D(\psi_t) \leq \min\{ n, m_t \}$ we look for
\begin{align}
    \hat{\chi}_t = \arg\max_{\chi}\ Pr \{ \Theta_t^{(\phi_t, \psi_t)} | \mathbf{Z}_{1:t} \}.
\end{align}
Then we prune all $\chi$-hypotheses for given $\phi_t$ and $\psi_t$ except for the \\$\hat{\chi}_t$-hypothesis. Obviously, the resulting probabilities must be normalized after pruning.

Simulations performed in \cite{Blom_coalscence_avoiding} show that JPDA* outperforms PDA, JPDA, and ENNPDA in suppressing coalescence, while remaining robust to misdetections and clutter. To demonstrate the difference between these methods, Table \ref{tab:prunning_example} displays the manner in which each approach prunes the hypotheses in a particular scenario.

\begin{table}[h]
    \centering
    \begin{tabular}{|c|c||c|c|}
         $\Theta$ & $Pr\{ \Theta | \mathbf{Z}_{1:t} \}$ & ENNPDA & JPDA*\\
         \hline
         $[0,0]$ & 0.00011 & $\times$ & \\
         $[0,1]$ & 0.05758 & $\times$ & $\times$ in favor of $[1, 0]$\\
         $[0,2]$ & 0.31308 &  & \\
         $[1,0]$ & 0.11829 & $\times$ & \\
         $[1,2]$ & 0.17577 & $\times$ & $\times$ in favor of $[2, 1]$\\
         $[2,0]$ & 0.13885 & $\times$ & $\times$ in favor of $[0, 2]$\\
         $[2,1]$ & 0.19632 & $\times$ & \\
         \hline
    \end{tabular}
    \caption{Comparison of pruning of ENNPDA and JPDA* for two targets and two received measurements. Symbol $\times$ represent that the corresponding hypothesis was pruned by the algorithm.}
    \label{tab:prunning_example}
\end{table}

\section{Coupling}
One of the approaches to tackle the colaescence is to make use of the concept of a joint state for multiple targets. For example, JPDAC \cite{barshalom1995multitarget}. The effectiveness of the JPDAC approach in combination with two other Bayesian approaches (imaging sensor filter and interactive multiple model - IMM, respectively) has been demonstrated for closely spaced target situations in \cite{shertukde1991tracking}, \cite{bar1991tracking}. 

Another method that uses the idea of joint state is coupled PDA (CPDA) introduced in \cite{Blom_coalscence_avoiding}. Furthermore, the authors combined this approach with hypotheses pruning resulting in CPDA* \cite{Blom_coalscence_avoiding}. From their results, it is evident that hypothesis pruning appears to be the most efficient method to suppress the coalescence effect (CPDA* outperforms CPDA, CPDA* and JPDA* show similar performance). 

Evidence has been presented that ENNPDA, JPDA*, and CPDA* avoid track coalescence by positioning a Gaussian density around a local optimum (not necessarily the global optimum). This method seems so efficient that the requirement to recall the coupling between tracks practically disappears \cite{Blom_coalscence_avoiding}.

\raggedbottom
\section{Bias removal}\label{sec:bias_removal}
Another approach to coalescence suppression is based on the removal of bias from the posterior state estimate. The state estimate becomes biased when the targets approach each other and their validation regions intersect, since any track attempting to follow a particular target will be attracted, to some extent, towards a neighboring target with whose returns it correlates \cite{fitz1985biasmagnitude}. Furthermore, the magnitude of the tracking bias is quantitatively analyzed in \cite{fitz1985biasmagnitude}. The paper establishes that the bias increases as the distance between the targets decreases, reaching a maximum when the targets are very close to each other. Using a two-target scenario, the authors show that target separations smaller than a certain critical value tend to cause a total coalescence of the tracks. 

In the latter works, this paradigm was used for Bias Removal JPDA (BRJPDA) \cite{brjpda2015jing}. The authors analyze the causes of state bias in JPDA and provide a direct computation equation for the bias in an ideal two-target case. They then extend the bias estimation to more general and practical cases by defining target detection hypotheses and target-to-target association hypotheses. The estimated bias is subsequently removed from the state updated by JPDA to produce an unbiased state. 

However, as discovered by \cite{fitz1985biasmagnitude}, the root of bias lies in the measurements generated by adjacent tracks, which are used to update the current track. Therefore, to estimate the bias, it is essential to identify the sources of the given measurements and exclude those originating from neighboring tracks from our considerations, which is the main problem. If not, the bias estimation relies on an approximation that inherently holds uncertainty. However, the authors show that BRJPDA \cite{brjpda2015jing} can effectively handle track coalescence and offers performance similar to that of JPDA* and outperforms that of "vanilla" JPDA.

We examined prevalent methods for coalescence prevention in JPDA filters. Among these, hypotheses pruning, particularly JPDA*, appears to be the most straightforward and effective for this purpose. It surpasses strategies that rely on coupling, including the CPDA* that employs both coupling and hypothesis pruning. Essentially, hypothesis pruning forces the JPDA to make a decision \cite{brekke2018fundamentals}. We chose to expand on this concept by incorporating adaptive validation gating.

\section{Adaptive validation gating}\label{sec:our_method}
In this section we present our contribution to the field of track coalescence suppression. The approach leverages the concept of pruning introduced in JPDA* and dynamically adjusts the size of the validation gate to mitigate the risk of track coalescence.

The proposed method introduces an adaptive validation gating technique that scales the gating probability, $P_G$, based on the spatial proximity of the track validation ellipses. We introduced the gating probability in Section \ref{sec:PDA}. The default value of $P_G$ (which is typically equal to one or nearly so) is dynamically reduced toward some value denoted $P_{squeeze}$ when the validation ellipses of multiple tracks intersect, effectively shrinking the validation gate. This reduction in gate size aims to limit the association possibilities between closely spaced tracks, thus reducing the likelihood of coalescence. It is important to note that we only apply gate squeezing to pairs of gates that intersect in the current time scan $t$.

So, the gating process from \eqref{eq:elipsoidal_gating} is modified as follows
\begin{align}
    \mathcal{V}(\mathbf{Z}; \gamma(P_{G, t})) &= \{ \mathbf{z} \in \mathbf{Z} | (\mathbf{z} - \mathbf{\hat{z}}_{t|t-1})\mathbf{S}_t^{-1}(\mathbf{z} - \mathbf{\hat{z}}_{t|t-1}) <  \gamma(P_{G,t})\}.\label{eq:adaptive_elipsoidal_gating} \\
    \gamma(P_{G,t}) &=
    \begin{cases}
        F_{\chi^2_{df=l}}^{-1}(P_G) & \text{if no intersection is detected} \\
        F_{\chi^2_{df=l}}^{-1}(P_{squeeze}) & \text{for intersected gates},
    \end{cases}
\end{align}
where $ F_{\chi^2_{df=l}}^{-1}(\cdot)$ stands for inverse CDF function, or percentile function of $\chi^2$ distribution with $l$ degrees of freedom, where $l$ is the length of the measurement vector. In Figure \ref{fig:chi_2_ppf}, the percentile function of the $\chi^2$ distribution with two degrees of freedom is depicted. It is evident that even a minor adjustment in the desired probability significantly reduces the output of the function. 
\begin{figure}
    \centering
    \includesvg[width=\textwidth]{figures/04_01_chi2_percentile.svg}
    \caption{$\chi^2$ with two degrees of freedom percentile function.}
    \label{fig:chi_2_ppf}
\end{figure}

Reducing the probability of gating has consequences. To prevent excessive gate shrinkage and the subsequent risk of missed detections, a control mechanism is implemented. Two key parameters govern this mechanism.
\begin{itemize}
    \item \verb|squeeze_threshold| This parameter defines the maximum number of consecutive time steps during which gate squeezing can be applied. Once this threshold is reached, the gate size is no longer reduced.
    \item \verb|squeeze_cooldown| This parameter specifies the number of time steps during which gate squeezing is prohibited after reaching the \\  \verb |squeeze_threshold|. This cooldown period allows tracks to diverge and reduces the risk of track loss due to overly restrictive gating.
\end{itemize}
The algorithm operates as follows.
\begin{enumerate}
    \item \textbf{Intersection Detection} At each time step, the algorithm checks for intersections between the validation ellipses of all active tracks.
    \item \textbf{Gate Size Adaptation} If intersections are detected, the value of $P_{G_t}$ for the corresponding tracks is reduced, affecting $\gamma(P_{G,t})$, effectively shrinking the validation gates.
    \item \textbf{Threshold and Cooldown Management} The algorithm tracks the number of consecutive squeezing instances. If the \verb|squeeze_threshold| is reached, gate squeezing is halted for the duration specified by the \verb|squeeze_cooldown| parameter.
    \item \textbf{Hypothesis pruning} Further reduction of hypotheses by implementing the JPDA* pruning presented in Section \ref{sec:JPDA_star}.
\end{enumerate}
Pseudocode is presented in Algorithm \ref{alg:AVGHP}.
\begin{algorithm}
    \caption{Adaptive Validation Gating with Hypothesis Pruning}
    \label{alg:AVGHP}
    \begin{algorithmic}[1]
        \Require{Tracks $T$, measurements $Z$, gating probability $P_G$, reduced gating probability $P_{squeeze}$, \verb|squeeze_threshold| $S_T$, \verb|squeeze_cooldown| $S_C$}
        \Ensure{Updated tracks with reduced coalescence}
        \State Initialize $squeeze\_count \gets 0$ for each track
        \For{each time step $t$}
            \For{each track $T_i$}
                \State Generate validation gate based on $P_G$
                \For{each pair of tracks $T_i$, $T_j$ with intersecting gates}
                    \If{$squeeze\_count[T_i] < S_T$ and $squeeze\_count[T_j] < S_T$}
                        \State $P_G[T_i] \gets P_{squeeze}$
                        \State $P_G[T_j] \gets P_{squeeze}$
                        \State $squeeze\_count[T_i] \gets squeeze\_count[T_i] + 1$
                        \State $squeeze\_count[T_j] \gets squeeze\_count[T_j] + 1$
                    \EndIf
                \EndFor
                \State Associate measurements within the gate to $T_i$
                \State Calculate association probabilities using JPDA
            \EndFor
            \State Perform JPDA* hypothesis pruning 
            \State Update tracks using JPDA
            \For{each track $T_i$ with $squeeze\_count[T_i] \geq S_T$}
                \If{$cooldown\_timer[T_i] = 0$}
                    \State $squeeze\_count[T_i] \gets 0$
                    \State $cooldown\_timer[T_i] \gets S_C$
                \Else
                    \State $cooldown\_timer[T_i] \gets cooldown\_timer[T_i] - 1$ 
                \EndIf
            \EndFor
        \EndFor
    \end{algorithmic}
\end{algorithm}

This adaptive validation gating method offers several potential benefits for JPDA filtering
\begin{itemize}
    \item \textbf{Reduced Track Coalescence} By dynamically adjusting the validation gate size, the proposed method aims to minimize the likelihood of track coalescence, particularly in scenarios with closely spaced objects.
    \item \textbf{Improved Tracking Accuracy} By mitigating track coalescence, the method is expected to enhance the overall tracking accuracy and maintain the individual identities of closely spaced objects.
    \item \textbf{Adaptive and Responsive Behavior} The use of thresholds and \\cooldown periods ensures that the gate size adaptation is responsive to the dynamic nature of the tracking environment, preventing excessively restrictive gating and potential track loss.
\end{itemize}

This section presented a novel approach to track coalescence suppression in JPDA filters. The subsequent sections of the thesis will delve into a detailed analysis of the method's performance through simulations and experiments. These analyses will evaluate the effectiveness of the proposed approach in comparison to existing techniques and explore the impact of various parameter choices on tracking performance. Furthermore, the limitations of the method and potential avenues for future research will be discussed.
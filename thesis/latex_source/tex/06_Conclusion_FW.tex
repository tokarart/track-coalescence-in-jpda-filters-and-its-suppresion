\chapter{Conclusion and Future Work}

This thesis has explored the challenges and solutions associated with track coalescence in Joint Probabilistic Data Association (JPDA) filters, a widely used class of multi-target tracking algorithms. We began by establishing a comprehensive understanding of single-target tracking (STT) principles, including state estimation, motion models, and the impact of measurement noise and clutter. The limitations of traditional Kalman filters in handling clutter led to the introduction of the Probabilistic Data Association (PDA) filter, which effectively incorporates data association probabilities to mitigate the adverse effects of clutter and missed detections.

Moving into the realm of multi-target tracking (MTT), we delved into the complexities of tracking multiple objects simultaneously and the critical issue of track coalescence. JPDA filters, as an extension of PDA for MTT scenarios, were analyzed in detail, highlighting their vulnerability to track coalescence when targets move in close proximity. The factors contributing to track coalescence, including high clutter density, low detection probability, and similar target motion, were discussed, along with the detrimental consequences of track loss, degraded state estimation, and inaccurate target identification.

To address the challenge of track coalescence, we investigated various existing suppression techniques, including:
\begin{itemize}
    \item \textbf{Exact Nearest Neighbor (ENNPDA)} A simple yet effective approach that prunes all but the most likely association hypothesis, leading to reduced coalescence but increased sensitivity to clutter and misdetections.
    \item \textbf{JPDA* } A more robust approach that retains the clutter and misdetection resilience of JPDA while strategically pruning less likely hypotheses to mitigate coalescence.
    \item \textbf{Coupling Methods (e.g., CPDA, CPDA*)} Techniques that utilize a joint state representation for multiple targets, offering some improvement in handling coalescence, but often with increased complexity.
    \item \textbf{Bias Removal Methods (e.g., BRJPDA)} Approaches that estimate and remove bias from the state estimates to counteract the tendency of tracks to merge during close interactions.
\end{itemize}

Building upon the concept of hypothesis pruning in JPDA*, we introduced a novel method that incorporates adaptive validation gating to further enhance coalescence suppression. Our proposed algorithm dynamically adjusts the validation gate size based on the proximity and estimated states of nearby targets. By shrinking the gate during potential track interactions, we aim to limit association possibilities and reduce the likelihood of tracks merging erroneously.

To evaluate the performance of our proposed method, we conducted simulations and experiments.
\begin{itemize}
    \item \textbf{Monte Carlo Simulations} A grid search approach was employed to identify the optimal parameter settings for our adaptive gating mechanism, considering the trade-offs between localization accuracy, cardinality errors, and misdetections.
    \item \textbf{Controlled Experiment with Fixed Seeds} A comprehensive comparison of JPDA, JPDA*, and our proposed method was conducted across a wide range of scenarios with varying clutter densities, measurement uncertainties, target proximities, and trajectory types. The results provided insights into the relative performance and robustness of each algorithm under different conditions. 
    \item \textbf{Direct Comparison in Specific Scenarios} We directly compared the algorithms in three concrete scenarios that represent easy, medium and hard tracking conditions. This allowed us to assess their behavior and effectiveness in handling specific challenges.
\end{itemize}

The experimental results demonstrate that our proposed JPDA* variant with adaptive gating generally outperforms both the baseline JPDA and JPDA* with pruning. Our method consistently achieves:
\begin{itemize}
    \item \textbf{Improved Localization Accuracy} The adaptive gating mechanism effectively mitigates track coalescence, leading to more accurate state estimation and reduced localization errors (RMSE) compared to the other algorithms, especially in scenarios with moderate difficulty.
    \item \textbf{Enhanced Overall Tracking Performance} By balancing the trade-off between localization and cardinality errors, our method achieves the best overall tracking performance (GOSPA) among the algorithms tested.
\end{itemize}
However, our approach exhibits a drawback in terms of a higher misdetection rate in extreme or challenging scenarios. While the median number of misdetections remains low, indicating reliable track maintenance in most situations, specific conditions can lead to increased missed detections due to the potentially aggressive nature of adaptive gating.

This limitation paves the way for future research and improvements. 
\begin{itemize}
  \item \textbf{Adaptive} \verb|P_squeeze|: Our current implementation uses a predefined value for \verb|P_squeeze|, the probability used to shrink the validation gate. To further enhance the adaptability of the algorithm, \verb|P_squeeze| could be dynamically adjusted based on factors such as:
    \begin{itemize}
      \item \textbf{Target Proximity} A more nuanced approach could scale \\\verb|P_squeeze| based on the distance between intersecting gates or the estimated distance between targets, allowing for a more sophisticated control of the gate size.
      \item \textbf{Track Quality Metrics} \verb|P_squeeze| could be adapted based on estimated track quality or confidence level, providing a more informed gating decision that considers the reliability of the tracks involved.
      \item \textbf{Clutter Density and Measurement Noise} The gating probability could be dynamically adjusted to account for varying levels of clutter and measurement noise, ensuring a more robust response to changing environmental conditions.
    \end{itemize}
  \item \textbf{Alternative Gating Strategies} Exploring alternative gating shapes, such as elliptical or rectangular gates with dynamically adjusted dimensions, could offer additional flexibility and potentially improve performance in specific scenarios.
  \item \textbf{Advanced Track Management} Integrating more sophisticated track management strategies that consider track quality, age, and the likelihood of misdetections could further enhance track maintenance and reduce the impact of missed detections on overall tracking performance.
  \item \textbf{Integration with Machine Learning:} Incorporating machine learning techniques to learn optimal gating parameters or predict track coalescence events could lead to more intelligent and adaptive tracking systems.
\end{itemize}

By addressing the limitations of our proposed method and exploring these avenues for future research, we can strive towards even more robust, accurate, and adaptable multi-target tracking solutions capable of effectively handling complex and challenging scenarios with closely spaced targets. 
\chapter{Introduction} \label{ch:intro}

Target tracking is the fundamental process of determining the location and trajectory of one or more moving objects over time using a series of measurements. It is a complex task that involves the ability to detect potential targets, separate true targets from false alarms or clutter, associate sequential measurements to the appropriate targets, and then estimate the target's state. Target states typically include kinematic components such as position and velocity and may encompass other attributes such as size, shape, or classification.

Target tracking finds widespread and often critical applications in a multitude of domains:
\begin{itemize}
    \item \textbf{Microscopic Applications} \cite{MTTcellmicroscopy}, \cite{chen2006automated} In microbiology and other scientific fields, target tracking is used to study the movement and behavior of cells, bacteria, and other microscopic organisms. This type of tracking provides crucial information for research on diseases, cell function, and biological processes.
    \cite{choi2013autonomousdriving} Self-driving vehicles and various robotic systems depend on target tracking for navigation, obstacle avoidance, and decision making. The need to perceive and track dynamic elements in the environment is a core requirement for autonomy. 
    \item \textbf{Wildlife Monitoring} \cite{lopez2014ArgosDoppler} Target tracking is used in ecological studies and wildlife preservation. Researchers use it to track animal movement patterns, migration routes, and population densities. These data help them understand animal behavior, habitat selection, and support conservation efforts.
    \item \textbf{Aerospace and Defense} \cite{Blackmanaerospace} Essential for air traffic control, missile guidance systems, early warning systems, and threat assessment within military operations. Tracking systems are crucial for the safe and efficient navigation of aircrafts and to protect assets from potential danger.
    \item \textbf{Surveillance} \cite{benford2011surveillance} systems rely on target tracking for perimeter security, crowd monitoring, incident detection, and crime prevention. The ability to track individuals or vehicles plays an important role in ensuring safety in both public and private settings.
\end{itemize}

Filtering algorithms form the foundation of target tracking methodologies. The word \textit{filter} in this context originates from its use in signal processing and electrical engineering. In these fields, a filter is a system or process that selectively modifies, removes, or enhances specific aspects of a signal. Early tracking algorithms, particularly the Kalman Filter \cite{Kalman}, were built on the concepts of extracting meaningful information (in this case, the true state of the target) from noisy and imperfect data. This mirrors the action of an electrical filter that can separate a desired frequency range from a complex signal. 

Filters are algorithms that dynamically estimate the state of a target in the presence of uncertainty and noise. These filters work by combining imperfect sensor measurements with a mathematical model that describes the expected motion patterns of the target. Here is a brief overview of some of the most widely used filters in target tracking.
\begin{itemize} 
    \item \textbf{Kalman Filter (KF)} The Kalman Filter \cite{Kalman} is the cornerstone of filtering techniques. It is designed for linear systems and Gaussian noise distributions. The KF works by recursively updating the target state estimate and its associated uncertainty.
    \item \textbf{Extended Kalman Filter (EKF)} Designed for non-linear systems. The EKF linearizes the non-linear system model around the current state estimate. This linearization allows us to apply the principles of the Kalman Filter to the non-linear system.
    \item \textbf{Particle Filter (PF)} Particle filters \cite{del1997nonlinear} excel in handling non-linear and non-Gaussian scenarios. They approximate the probability distribution of the target state using a set of weighted samples (particles). PFs are computationally more intensive, but their flexibility in modeling complex scenarios makes them invaluable for certain tracking applications.
    \item \textbf{Unscented Kalman Filter (UKF)} UKF \cite{ukf2000wan} employs deterministic sampling to handle non-linearities, potentially offering improved accuracy over the EKF.
\end{itemize}
The choice of a particular filter largely depends on the characteristics of the system being tracked (for example, its dynamics and noise models) and the computational resources available for the task.

Target tracking poses numerous challenges that modern algorithms seek to address. They must deal with \textbf{noisy measurements} that introduce uncertainty in the estimation of the state of a target. \textbf{Errors in sensor readings} of a target's position, velocity, or other attributes can propagate and degrade the accuracy of the track. Furthermore, the tracking task is made more difficult by the presence of \textbf{clutter} in the sensor data. Clutter can originate from background noise, reflections, natural phenomena, or deliberate countermeasures, leading to \textbf{false detections} that obfuscate the identification of true targets. Furthermore, no sensor is perfect, even the most advanced detectors can occasionally miss true targets (resulting in false negatives) or mistake a clutter source for an actual target (introducing false positives). Such misdetections can create confusion and disrupt the continuity of a target's established track.

These challenges underscore the importance of designing robust target tracking techniques. Robustness in this context means resilience - the ability of the tracking system to maintain reliability and generate accurate estimates, even when faced with the inevitable imperfections of noise, clutter, and potential misdetections.

While the Kalman Filter and its variants provide a foundation for target tracking, they can be susceptible to disruption under challenging conditions. Although they handle noisy measurements and occasional misdetections, clutter represents a significant source of failure. To tackle single-target tracking in the presence of clutter, algorithms like the following are employed:
\begin{itemize}
    \item \textbf{Nearest Neighbor (NN)} \cite{basicNNF} Chooses the best data association and prunes all others in a greedy manner to approximate the posterior. This has the disadvantage of underestimating the posterior uncertainty. 
    \item \textbf{Probabilistic Data Association (PDA)} \cite{BARSHALOM1975451} Instead of selecting only the most probable association hypothesis, the PDA filter merges all valid hypotheses into the posterior weighted by their likelihoods. This approach acknowledges uncertainties better than NN. 
    \item \textbf{Multiple Hypothesis Tracking (MHT)} \cite{reidMHT} Maintains multiple potential track hypotheses over time, reducing the risk of track loss due to misassociations.
\end{itemize}

For the more complex problem of multi-target tracking, two major families of robust tracking algorithms exist:
\begin{itemize}
    \item \textbf{Associative Filters} These filters rely on establishing associations between measurements and existing tracks. Notable examples include \textbf{MHT}, \textbf{Joint Probabilistic Data Association (JPDA)} \cite{barshalom2009PDA}, and \textbf{Global Nearest Neighbor (GNN)} approaches.
    \item \textbf{Random Finite Set (RFS)} based filters. A different mathematical framework that models the set of targets directly. Examples include \textbf{Probability Hypothesis Density (PHD)} \cite{voPHD}.
\end{itemize}

While the power and sophistication of RFS based filters offer advantages in complex scenarios, their in-depth discussion falls outside the scope of this thesis.

A significant challenge unique to multi-target tracking, particularly in the context of associative filters, is the potential for track coalescence. This phenomenon occurs when multiple targets move in close proximity, causing their measurements to overlap and become difficult to distinguish. The filter may converge onto a single track, as shown in Figure \ref{fig:coalescence_example}, representing the center of the group, rather than maintaining distinct tracks for each individual target.

\begin{figure}[h]
    \centering
    \includesvg[width=\textwidth]{figures/01_coalscence_example.svg}
    \caption{Coalescence example.}
    \label{fig:coalescence_example}
\end{figure}
Track coalescence disrupts accurate count and state estimation of targets, which is crucial for applications that require situational awareness. It becomes progressively more pronounced in scenarios with dense clutter and as the number of closely spaced targets increases. 

In this thesis, we focus on suppressing the coalescence effect in JPDA filters. This effect is widely recognized, and numerous diverse techniques have been developed for its mitigation. The most common are the exact nearest-neighbor PDA (ENNPDA) \cite{ennpda_fitzgerald}, its extensions JPDA* and CPDA* \cite{Blom_coalscence_avoiding} based on hypothesis pruning. Some methods make use of the concept of a joint state for multiple targets. For example, JPDAC \cite{barshalom1995multitarget} or coupled PDA (CPDA) introduced in \cite{Blom_coalscence_avoiding}.

In the following chapters, we will dive into the aspects of single-target and multi-target tracking. In Chapter \ref{ch:coalescence_suppression} we discuss the coalescence suppression approaches specifically for JPDA and present our method. Chapter \ref{ch:experiment_validation} describes the experimental validation of the suggested method.
\chapter{Experiment validation}\label{ch:experiment_validation}
This chapter focuses on the evaluation and comparison of three JPDA-based algorithms: the standard JPDA from Section \ref{sec:JPDA}, JPDA* from Section \ref{sec:JPDA_star}, and a novel variant we propose, JPDA* with adaptive gating from Section \ref{sec:our_method}. Our proposed method enhances the JPDA* filter by dynamically adjusting the validation gate size based on the proximity and estimated states of nearby targets. This adaptive gating mechanism aims to further reduce the likelihood of track coalescence while maintaining tracking accuracy.

The goal of this chapter is twofold. Firstly, our objective is to identify the optimal parameters for our method using Monte Carlo simulations. Secondly, we will conduct a comprehensive comparison of the three algorithms across different tracking scenarios to assess their performance and coalescence mitigation capabilities. Through this analysis, we aim to demonstrate the effectiveness of our proposed method and its potential for improved multi-target tracking in challenging environments. Subsequently, we will evaluate these algorithms across three distinct scenarios, each of which presents varying levels of complexity.

\section{Methodology}
Three algorithms were implemented in Python, using the NumPy and SciPy libraries. All filters use the constant velocity model presented in Section \ref{subsec:CVM} with a 4 dimensional state $\mathbf{x} = [x,\ v_x,\ y,\ v_y ]^\intercal$. The measurement model matrix $\mathbf{H}$ is compatible with this state. For the process noise covariance we use
\begin{align}
    \mathbf{Q} &= 
    \begin{bmatrix}
        0.1 & 0 & 0 & 0 \\
        0 & 1 & 0 & 0 \\
        0 & 0 & 0.1 & 0 \\
        0 & 0 & 0 & 1 \\
    \end{bmatrix},
\end{align}
anticipating greater variability in velocities compared to positions. The implementations are available on the attached media. All simulations were executed on MacBook Pro 2020 with M1 chip with 
\begin{itemize}
    \item 8 CPU cores,
    \item 3.2 GHz maximum CPU clock rate,
    \item 8 GB RAM.
\end{itemize}

\subsection*{Dataset}
We created four synthetic trajectories for the simulations shown in Figure \ref{fig:traj_types}. All targets move with constant velocity. The base trajectory for one target is created using blender software and then mirrored according to the parameter \verb|mirror_offset| that defines the distance between measurements at the closest point for two tracks as shown in Figure \ref{fig:mirror_offset_demo}. All trajectories are centered around $x = 100$.

\begin{figure}
    \centering
    \includesvg[width=\textwidth]{figures/05_01_all_traj.svg}
    \caption{Synthetic trajectories for simulations.}
    \label{fig:traj_types}
\end{figure}

\begin{figure}
    \centering
    \includesvg[width=\textwidth]{figures/05_02_mirror_offset.svg}
    \caption{Impact of mirror offset on trajectory.}
    \label{fig:mirror_offset_demo}
\end{figure}

A \verb|Radar| class has been developed to emulate the data flow. In addition to setting the trajectory type and \verb|mirror_offset|, it allows configuration of the clutter rate $\lambda$, the dimensions of the surveillance area, and the measurement covariance matrix $\mathbf{R}$ (see Figures \ref{fig:lambda_demo} and \ref{fig:R_demo}). 

\begin{figure}
    \centering
    \includesvg[width=\textwidth]{figures/05_03_clutter_rate.svg}
    \caption{Data from simulator trough all time scans for different parameter $\lambda$.}
    \label{fig:lambda_demo}
\end{figure}

\begin{figure}
    \centering
    \includesvg[width=\textwidth]{figures/05_04_R.svg}
    \caption{Trajectories for different $\mathbf{R}$.}
    \label{fig:R_demo}
\end{figure}

\subsection*{Metrics}\label{subsec:metrics}
To evaluate and compare the performance of the JPDA, JPDA*, and our method with adaptive gating algorithms, we employ two primary metrics: Root Mean Square Error (RMSE) and Generalized Optimal Sub-Pattern Assignment (GOSPA) metric \cite{GOSPA}.

RMSE is a commonly used metric for evaluating the precision of state estimation. It measures the average Euclidean distance between the estimated target positions and their true positions. For a set of $N$ targets and $T$ time steps, the RMSE is calculated as:
\begin{align}
    RMSE &= \sqrt{\frac{1}{NT} \sum_{n=1}^{N} \sum_{t=1}^{T} ||\hat{\mathbf{x}}_n^t - \mathbf{x}_n^t||^2},
\end{align}
where $\hat{\mathbf{x}}_n^t$ and $\mathbf{x}_n^t$ represent the estimated and true positions of target $n$ at time step $t$, respectively.

Although RMSE provides a simple measure of estimation accuracy, it does not account for errors in the number of tracks or data association mistakes. This limitation makes it insufficient for comprehensive evaluation of multi-target tracking algorithms, especially in scenarios prone to track coalescence.

The generalized optimal sub-pattern assignment or GOSPA metric \cite{GOSPA} overcomes the limitations of RMSE by considering both localization errors and cardinality errors (missed and false tracks) in a unified framework. This is achieved by finding the optimal assignment between the set of true target states and the set of estimated tracks, considering penalties for missed targets, false tracks, and localization errors.
The GOSPA metric is defined as follows:
\begin{align}
    GOSPA(X,Y) = \left( \min_{\pi \in \Pi_p} \sum_{i=1}^{m} d_c^{(c)}(x_i, y_{\pi(i)})^p + \frac{c^p}{p}(n-m) \right)^{1/p},
\end{align}
where:
\begin{itemize}
    \item $X = {x_1, ..., x_m}$ is the set of true target states.
    \item $Y = {y_1, ..., y_n}$ is the set of estimated tracks.
    \item $m$ and $n$ are the number of true targets and estimated tracks, respectively.
    \item $\Pi_p$ is the set of all possible assignments between $X$ and a subset of $Y$ with at most $p$ elements.
    \item $d_c^{(c)}(x,y) = \min(c, d(x,y))$ is the cut-off distance function, where $d(x,y)$ is the distance between states $x$ and $y$, and $c$ is a cut-off parameter.
    \item $p$ determines the penalty for cardinality errors relative to localization errors.
\end{itemize}
It is necessary to define the parameters $c$ and $p$. The GOSPA metric provides a more comprehensive evaluation of tracking performance compared to RMSE, making it suitable for analyzing the behavior of JPDA-based algorithms and their ability to handle track coalescence. 

The final metric to be monitored is the number of misdetections reported by the algorithm. This could occur when the validation gate for a particular target is empty, causing the algorithm to omit the update step and declare a misdetection. Given that GOSPA conducts its own track-to-measurement assignment, the reported misdetections by the algorithm may vary from the cardinality errors identified by GOSPA.


\section{Parameter Optimization via Monte Carlo Simulations}
To optimize the performance of our method, we employ Monte Carlo simulations coupled with a grid search approach. This process involves evaluating the algorithm's performance across a range of parameter combinations and selecting the set that yields the best results. We optimize the following parameters.
\begin{itemize}
    \item \verb|P_squeeze|: The shrunken probability for the validation gate applied during potential track intersection scenarios.
    \item \verb|squeeze_max_steps|: The maximum number of consecutive time steps for which the squeezed validation gate is applied.
    \item \verb|cooldown_steps|: The number of time steps during which the standard validation gate is used after reaching the \verb|squeeze_max_steps| limit.
\end{itemize}
For each combination of parameter values within the defined grid, we run the algorithm 100 times on the chosen trajectory. This allows us to account for the stochastic nature of the tracking process and obtain statistically meaningful results. 

Before running simulations, it is essential to establish the simulation parameters, specifically, clutter rate $\lambda,\text{ measurement uncertainty }\mathbf{R}$, detection and gating probability $P_D \text{ and } P_G$, the type of trajectory, and the critical distance between tracks denoted by \verb|mirror_offset|. The conditions should neither be overly challenging for the filter nor too simplistic. Through empirical methods, we have identified the parameters under which the filter encounters some challenges but does not immediately lose track. These parameters are as follows.
\begin{itemize}
    \item $\lambda = 0.0015$,
    \item $\mathbf{R} = 5\mathbf{I}$,
    \item Trajectory type -- \textbf{ trapezoid},
    \item \verb|mirror_offset| = 2,
    \item $P_D = 1$,
    \item $P_G = 0.99$
\end{itemize}
The last thing to determine for this scenario are the GOSPA parameters: cutoff distance $c$ and penalization term $p$. Cut-off distance represents the maximum distance beyond which two points are considered completely dissimilar or as a misdetection. Normally, it would depend on the physical properties of the given radar system and its resolution. Furthermore, if targets are known to be well-separated, a smaller $c$ can be used to emphasize localization accuracy. Nonetheless, a smaller $c$ could result in a uniform metric result, since the minimum is taken between the distance and $c$. The penalization term $p$ controls the sensitivity of the metric to outliers and the relative weighting of cardinality and localization errors. Since all of the simulated tracks are moving with constant velocity and have predictable trajectory, we expect the predictions to be quite accurate, co there is no need to set $c$ some high value.

In contrast, as discussed in Section \ref{sec:our_method}, adaptive gating in our approach could lead to a higher incidence of misdetections as a consequence of decreasing $P_G$. This leads to the increased value of the penalization term in order to reduce the misdetection rate. We set
\begin{itemize}
    \item $c = 20$,
    \item $p = 2$.
\end{itemize}
It is important to note that the optimal parameters may vary depending on the specific characteristics of the tracking scenario, including target dynamics, clutter density, and sensor capabilities. As such, the parameter optimization process should be tailored to the specific application context.

\subsection*{Results}
\begin{figure}[b]
    \begin{subfigure}{.49\textwidth}
        \centering
        \includesvg[width=\textwidth]{figures/05_07_RMSE_vs_params.svg}
        \label{fig:RMSE_vs_params}
    \end{subfigure}
    \begin{subfigure}{.49\textwidth}
        \centering
        \includesvg[width=\textwidth]{figures/05_06_GOSPA_vs_params.svg}
        \label{fig:GOSPA_vs_params}
    \end{subfigure}
    \caption{RMSE and GOSPA for different parameters.}
    \label{fig:rmse_gospa_MC}
\end{figure}

\begin{figure}
    \centering
    \includesvg[width=.6\textwidth]{figures/05_05_misdetections_vs_params.svg}
    \caption{Misdetections for different parameters.}
    \label{fig:misdetections_params_MC}
\end{figure}

\begin{figure}
    \centering
    \includesvg[width=\textwidth]{figures/05_08_correlation_heatmap.svg}
    \caption{Correlation heatmap.}
    \label{fig:heatmap_params}
\end{figure}
The plots presented in Figure \ref{fig:rmse_gospa_MC} illustrate the relationship between the adaptive gating parameters (\verb|P_squeeze|, \verb|squeeze_max_steps|, \verb|cooldown|) and the performance metrics RMSE and GOSPA. First, let us look at \verb|P_squeeze|. As \verb|P_squeeze| increases (larger validation gate), both RMSE and GOSPA exhibit a general upward trend. This suggests that a more permissive gating approach leads to increased localization errors (RMSE) and potentially more cardinality errors (missed detections and false tracks reflected in GOSPA). This is likely due to the inclusion of more clutter and potentially incorrect data associations within the larger gate. The trade-off between localization accuracy and cardinality errors remains a key consideration.

With increasing \verb|squeeze_max_steps| (longer duration of the shrunken gate), RMSE shows a sharp decrease, indicating improved localization accuracy due to a more focused validation region during potential track interactions. GOSPA demonstrates a similar behavior. 

Both RMSE and GOSPA show relatively stable trends with increasing\\ \verb|cooldown| periods. This suggests that the \verb|cooldown| duration has a less significant impact on overall tracking performance compared to \verb|P_squeeze| and \verb|squeeze_max_steps|. 

Figure \ref{fig:misdetections_params_MC} displays the average number of misdetections. The number of misdetections decreases significantly with increasing \verb|P_squeeze|. Surprisingly, this does not correlate with the expected GOSPA trends. It is important to recognize that while GOSPA considers cardinality errors (including missed detections), it may not directly reflect the exact number of misdetections reported by the algorithm. This is due to several key factors:
\begin{itemize}
    \item \textbf{Independent Association} GOSPA performs its own optimal association between true targets and estimated tracks, which may differ from the associations made by the tracking algorithm itself. As a result, GOSPA might identify potential associations between tracks and measurements that the algorithm classified as misdetections, leading to a discrepancy in the reported misdetection counts.
    \item \textbf{Constant Validation Gate} GOSPA operates with a fixed cut-off distance $c$ for determining association possibilities, whereas the adaptive gating mechanism dynamically adjusts the size of the validation gate. This difference in gating strategies can further contribute to discrepancies between GOSPA's assessment of cardinality errors and the actual misdetections encountered by the algorithm.
\end{itemize}
However, Figure \ref{fig:heatmap_params} clearly illustrates that both RMSE and GOSPA exhibit a strong negative correlation with the number of misdetections, although the correlation strength is lower for GOSPA ($-0.688$ compared to $-0.834$ for RMSE). This indicates that while GOSPA does account for cardinality errors to some extent, their influence on the overall metric is minor relative to localization errors.

Misdetections show a clear upward trend with increasing \verb|squeeze_max_steps|, confirming that longer durations of the shrunken gate lead to a higher likelihood of missed detections. This is likely due to the exclusion of valid measurements when the gate is too restrictive for extended periods. The \verb|cooldown| parameter appears to have minimal impact on the frequency of misdetections.

A correlation heatmap depicted in Figure \ref{fig:heatmap_params} reinforces the observed trends. Both RMSE and GOSPA are strongly positively correlated with \verb|P_squeeze| and negatively correlated with \verb|squeeze_max_steps|. The parameter \verb|cooldown| has a negligible negative correlation with the metrics. The relationship between misdetections and algorithm parameters is consistent with previous findings, emphasizing that a higher \verb|P_squeeze| leads to fewer misdetections, and to a lesser degree, reducing \verb|squeeze_max_steps| also contributes to fewer missed targets.

Based on these observations, selecting the optimal parameter combination involves finding a balance between minimizing localization errors (RMSE) and cardinality errors (GOSPA + misdetections). The specific choice will depend on the application priorities and the relative importance of accurate state estimation versus correct track maintenance. For example, if minimizing localization errors is crucial, a lower \verb|P_squeeze| value and a higher \verb|squeeze_max_steps| might be preferred. Conversely, if ensuring correct track cardinality is of greater importance, a higher \verb|P_squeeze| and a shorter \\\verb|squeeze_max_steps| might be more suitable, accepting a slight increase in localization errors. The \verb|cooldown| parameter appears to have a less significant impact on performance and could be chosen based on other application-specific considerations.

For further simulations, we set
\begin{itemize}
    \item \verb|P_squeeze| $= 0.975$.
    \item \verb|squeeze_max_steps| $= 3$.
    \item \verb|cooldown| $= 2$
\end{itemize}


\section{Algorithm Comparison: Controlled Experiment with Fixed Seeds}\label{sec:alg_comparison}
Once we have defined the optimal parameters of our method for a given scenario, we can compare it with other methods. To provide a controlled comparison of the JPDA, JPDA*, and our method, we conduct an experiment where we eliminate the stochastic factor by fixing the random seed during simulation. This allows us to isolate the impact of algorithm design and parameter choices on tracking performance. 

This experiment is also based on grid search. The parameters for this grid search are 
\begin{itemize}
    \item \verb|trajectory_type| $\gets$ \{angle, curve, line, trapezoid\} (see Figure \ref{fig:traj_types}).
    \item \verb|mirror_offset| $\gets \{ 1,\ 2,\ 5,\ 10,\ 20  \} $.
    \item $\lambda \gets \{ 0.0001,\ 0.0005, 0.001, 0.003 \}$.
    \item $\mathbf{R} \gets \{ 0.5,\ 1,\ 5,\ 10 \}$.
\end{itemize}
This gives us 240 combinations in total for each algorithm. 

\subsection*{Results}
\begin{figure}
    \centering
    \includesvg[width=\textwidth]{figures/05_09_correlation_heatmap_three_alg.svg}
    \caption{Correlation heatmap.}
    \label{fig:heatmap_three_algorithms}
\end{figure}

Figure \ref{fig:heatmap_three_algorithms} displays a correlation heatmap that features the experiment parameters. The heatmap reveals intriguing relationships between algorithm performance (RMSE and GOSPA) and the different scenario parameters. RMSE shows a moderate positive correlation with $\lambda$ (clutter rate) and $\mathbf{R}$ (measurement uncertainty). This is expected, as higher clutter density and measurement noise generally lead to increased localization errors for all algorithms. The correlation is stronger with $\lambda$. GOSPA exhibits a stronger positive correlation with $\mathbf{R}$ than RMSE. Otherwise, we see a similar dependence. Surprisingly, RMSE shows a stronger positive correlation with the amount of misdetections, which is more expected from GOSPA. 

Interestingly, both RMSE and GOSPA show weak negative correlations with the \verb|mirror_offset| parameter. This suggests that as the distance between tracks increases, the performance of all algorithms tends to improve slightly. In particular, this performance enhancement is noticeable for trajectories \textit{line} and \textit{trapezoid}, where targets move along a straight line in close proximity. Under such conditions with an increased clutter rate $\lambda$ and $\mathbf{R}$ tracking becomes particularly impossible (refer to Figure \ref{fig:R_demo} where $\mathbf{R} = 10$).

However, in Figure \ref{fig:heatmap_three_algorithms}, we can clearly see that the \textit{angle} trajectory has the strongest positive correlation with RMSE and GOSPA among other trajectories. It could be caused by a sharp turn in the trajectory at a critical point. The implemented algorithms use CVM as a motion model, which is not well suited for such rapid changes in motion. The most complicated trajectory in terms of misdetections is \textit{curve}, which is expected. Again, the reason is the constant velocity motion model and the highly maneuvering nature of the trajectory. 

The heatmap also provides subtle hints about the relative performance of the algorithms. The correlation patterns for baseline JPDA, JPDA*, and our proposed method appear quite similar. This indicates that overall performance trends with respect to the scenario parameters are consistent across all three algorithms. However, slight differences in the magnitude of the correlations can be observed. For example, the negative correlation for GOSPA and RMSE seems slightly stronger for our proposed method compared to JPDA and JPDA* with pruning, both of which have a low positive correlation. This could imply that our approach potentially exhibits improved performance in various tracking scenarios.

\begin{figure}
    \centering
    \includesvg[width=\textwidth]{figures/05_10_comparison_of_metrics.svg}
    \caption{Metrics for JPDA, JPDA* and our method.}
    \label{fig:metrics_three_algorithms}
\end{figure}

Figure \ref{fig:metrics_three_algorithms} displays three boxplots representing the metrics for three different algorithms. We see that JPDA* shows an increase in mean RMSE compared to baseline JPDA. However, we can see that the median RMSE for JPDA* is $32.7\%$ lower than for the baseline JPDA, which means that the error of JPDA* might be higher in extreme cases, but remains pretty low in normal conditions. Our method achieves the lowest mean and median RMSE values, indicating the best localization performance among the three algorithms. This implies that the adaptive gating mechanism further reduces the impact of track coalescence on state estimation, resulting in more accurate target tracking.

The baseline JPDA shows the highest median and mean GOSPA values. This suggests that the baseline JPDA suffers from a combination of localization errors and cardinality errors (missed detections and false tracks), leading to suboptimal overall tracking performance.
JPDA* exhibits a slight improvement in GOSPA compared to baseline JPDA, with lower mean and median values. This indicates that pruning helps reduce cardinality errors to some extent, but the improvement is not as significant as the reduction in RMSE.
The proposed method demonstrates the lowest median and mean GOSPA values, which represents the best overall tracking performance among the three algorithms. This implies that the adaptive gating approach effectively balances localization accuracy and cardinality errors, leading to more reliable and accurate multi-target tracking.

However, we can see an increase in the number of misdetections for our approach, compared to JPDA and JPDA*. Both show similar performance in terms of misdetections. 
\begin{table}[h]
  \centering
  \begin{tabular}{|c|c|c|c|}
    \hline
     & JPDA & JPDA* & Ours \\ \hline
    Mean   & 0.52 & 0.53 & 4.81 \\ \hline
    Median & 0 & 0 & 0 \\ \hline
    75th percentile & 0 & 0 & 1 \\ \hline
    90th percentile & 1 & 1 & 4 \\ \hline
  \end{tabular}
  \caption{Mean Misdetections for Three Algorithms}
  \label{tab:misdetections}
\end{table}
In Table \ref{tab:misdetections} we can see that baseline JPDA and JPDA* exhibit very low misdetection rates across all percentiles. The median and 75th percentile values of 0 indicate that in most scenarios, these algorithms experience no misdetections. Even at the 90th percentile, the number of misdetections remains low, 1 for both algorithms. This confirms their effectiveness in maintaining track continuity and avoiding track loss.
Our proposed method with adaptive gating, while still having a median misdetection value of 0, shows a significantly higher number of misdetections at higher percentiles. The 75th percentile value of 1 suggests that in a quarter of the scenarios, our method experiences at least one misdetection. This increases to 4 misdetections at the 90th percentile, indicating a higher susceptibility to missed detections in certain challenging situations. The data suggest that the increased misdetection rate in our proposed method is not a uniform issue across all scenarios but rather concentrated in a subset of challenging situations. While most scenarios still result in no misdetections, specific conditions lead to a higher number of missed tracks. 

\section{Direct Algorithm Comparison in Specific Scenarios}

To further evaluate the performance of the JPDA, JPDA*, and our proposed method with adaptive gating, we conduct a direct comparison in three concrete tracking scenarios with varying levels of difficulty. This allows us to assess how each algorithm handles specific challenges and observe their behavior under controlled conditions.

We define three scenarios with different parameter settings to represent \textit{easy}, \textit{medium}, and \textit{hard} tracking conditions. These scenarios are designed to vary the levels of measurement uncertainty, clutter density, and target proximity, which are key factors that influence track coalescence and overall tracking performance. The values of the specific parameters for each scenario are presented in Table \ref{tab:scenarios}.

\begin{table}[h]
    \centering
    \begin{tabular}{|c|c|c|c|}
        \hline
        Parameter             & Easy     & Medium   & Hard     \\ \hline \hline
        $\mathbf{R}$                     & 1.0       & 1.5       & 2.0       \\ \hline
        $P_D$                & 1.0       & 1.0       & 1.0       \\ \hline
        $P_G$                & 0.999     & 0.999     & 0.999     \\ \hline
        Trajectory           & Trapezoid  & Trapezoid  & Trapezoid  \\ \hline
        Offset                & 5         & 3         & 3         \\ \hline
        $\lambda$            & 0.001     & 0.0015    & 0.003     \\ \hline
        $P_{squeeze}$        & 0.975     & 0.975     & 0.975     \\ \hline
        Steps/Cooldown       & 3/2       & 3/2       & 3/2       \\ \hline
    \end{tabular}
    \caption{Parameter settings for the easy, medium, and hard tracking scenarios.}
    \label{tab:scenarios}
\end{table}

As shown in the table, the scenarios primarily differ in the following aspects:
\begin{itemize}
    \item \textbf{Measurement Uncertainty} ($\mathbf{R}$): The measurement noise covariance R increases from easy to hard scenarios, representing an increase in uncertainty in sensor measurements.
    \item \textbf{Clutter Density} ($\lambda$): The clutter rate $\lambda$ also increases from easy to hard scenarios, indicating a higher density of false alarms and background noise. 
    \item \textbf{Target Proximity} (Offset): The distance between targets at their closest point (offset) is smaller in the medium and hard scenarios compared to the easy scenario, creating a higher risk of track coalescence due to closer target interactions.

\end{itemize}

By comparing the algorithms across these scenarios, we can gain insights into their robustness and ability to handle different levels of difficulty and specific tracking challenges. 

\begin{figure}
    \centering
    \includesvg[width=.6\textwidth]{figures/easy_simulation_metrics.svg}
    \caption{Metrics for \textit{easy} scenario.}
    \label{fig:metrics_easy_scenario}
\end{figure}

\begin{figure}
    \centering
    \includesvg[width=.6\textwidth]{figures/medium_simulation_metrics.svg}
    \caption{Metrics for \textit{medium} scenario.}
    \label{fig:metrics_medium_scenario}
\end{figure}

\begin{figure}
    \centering
    \includesvg[width=.6\textwidth]{figures/hard_simulation_metrics.svg}
    \caption{Metrics for \textit{hard} scenario.}
    \label{fig:metrics_hard_scenario}
\end{figure}

\begin{figure}
    \begin{subfigure}{.32\textwidth}
        \centering
        \includesvg[width=\textwidth]{figures/easy_00152.svg}
        \label{fig:easy_traj}
        \caption{\textit{Easy} scenario.}
    \end{subfigure}
    \begin{subfigure}{.32\textwidth}
        \centering
        \includesvg[width=\textwidth]{figures/medium_00152.svg}
        \label{fig:medium_traj}
        \caption{\textit{Medium} scenario.}
    \end{subfigure}
    \begin{subfigure}{.32\textwidth}
        \centering
        \includesvg[width=\textwidth]{figures/hard_00152.svg}
        \label{fig:hard_traj}
        \caption{\textit{Hard} scenario.}
    \end{subfigure}
    \caption{Trajectories for different scenarios at time $t = 153$.}
    \label{fig:trajectories_last_timestamp}
\end{figure}

The graphs showing RMSE and GOSPA over time for each scenario (Figures \ref{fig:metrics_easy_scenario}, \ref{fig:metrics_medium_scenario}, and \ref{fig:metrics_hard_scenario}) provide valuable information on the comparative performance and behavior of JPDA, JPDA*, and our proposed method with adaptive gating under varying levels of difficulty. Furthermore, Figure \ref{fig:trajectories_last_timestamp} shows the trajectories generated by each algorithm in all three scenarios. Let us analyze the observations for each scenario.

All three algorithms exhibit similar performance in terms of both RMSE and GOSPA for the \textit{easy} scenario (see Figure \ref{fig:metrics_easy_scenario}). The metrics remain relatively stable and low throughout the scenario, indicating successful tracking of both targets with minimal localization and cardinality errors. This suggests that under certain tracking conditions with low measurement noise, clutter density, and sufficient target separation, all algorithms can effectively maintain track accuracy and avoid track coalescence. 

Concerning the \textit{medium} scenario, we observe the following.
\begin{itemize}
    \item JPDA and JPDA* with pruning show a significant increase in RMSE compared to the easy scenario, particularly after the point where the target trajectories approach each other. This indicates that both algorithms struggle with increased measurement uncertainty, clutter, and reduced target separation, leading to track loss.  
    \item Our proposed method with adaptive gating demonstrates significantly lower RMSE and GOSPA throughout the scenario, including during the critical track interaction phase. This highlights the effectiveness of adaptive gating in mitigating track coalescence and maintaining accurate state estimation even under more challenging conditions.
    \item GOSPA trends for all algorithms follow a similar pattern to the RMSE observations, further confirming the superior performance of our proposed method in terms of overall tracking accuracy and cardinality error management. Furthermore, Figure \ref{fig:metrics_medium_scenario} illustrates the difference in GOSPA between baseline JPDA and JPDA* during the last scans, while RMSE shows similar behavior. It indicates fewer cardinality errors for JPDA*.
\end{itemize}

For the \textit{hard} scenario, we see that all three algorithms experience a rapid increase in both RMSE and GOSPA shortly after the start of the scenario, indicating a loss of track accuracy and increased cardinality errors. The high level of measurement noise, clutter density, and close target proximity pose significant challenges for all algorithms, leading to track coalescence and ultimately track loss.
The observed decrease in RMSE during the period of closest target proximity is likely an artifact of the evaluation metric, rather than an indication of improved tracking performance. In Figure \ref{fig:trajectories_last_timestamp}, we can see that when the filter loses track, its predictions are concentrated in the center of the surveillance area. As the true target states approach each other in the center, even erroneous state estimates would result in lower RMSE values due to the reduced distance between the estimated and true positions. 
The subsequent increase in RMSE and GOSPA as the targets move apart further confirms the loss of track accuracy and the presence of significant cardinality errors.

The results across the three scenarios clearly demonstrate the advantages of our proposed JPDA* variant with adaptive gating in handling challenging tracking conditions. Although all algorithms perform well in the \textit{easy} scenario, our method exhibits superior robustness and accuracy in the \textit{medium} scenario, successfully mitigating track coalescence and maintaining reliable tracking performance. For a \textit{hard} scenario, our algorithm fails, as well as other ones. Concerning misdetections, no algorithm reported any misdetections in these scenarios.

These findings highlight the effectiveness of adaptive gating as a valuable technique to enhance JPDA-based tracking algorithms, particularly in scenarios with increased, but still manageable, measurement uncertainty, clutter density, and close target interactions. The ability to dynamically adjust the validation gate based on target proximity and estimated states enables our method to effectively balance the trade-off between localization accuracy and cardinality errors, leading to more robust and reliable multi-target tracking performance in complex and challenging environments. In extreme situations, however, our algorithm shows a higher degree of misdetections than others, which could be a foundation for future work.
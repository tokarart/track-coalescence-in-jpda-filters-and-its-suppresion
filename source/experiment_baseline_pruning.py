import gc
import pathlib
from time import sleep

import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm

from models.JPDA import JPDA
from models.JPDA_star import JPDA_star
from models.utils import calculate_gospa
from simulators.radar import RadarCustomTwoTargets

SEED = 1998
MC_ITERATIONS = 100


def run_montecarlo_experiment(curve_type, mirror_offset, lambd, R, JPDA_class):
    F = np.array([[1, 1, 0, 0],  # state transition matrix
                  [0, 1, 0, 0],
                  [0, 0, 1, 1],
                  [0, 0, 0, 1]])

    H = np.array([[1., 0, 0, 0],  # measurement matrix
                  [0., 0, 1, 0]])
    Q = np.diag([.1, .1, .01, .01])  # process noise covariance
    R = R * np.eye(2)  # measurement noise covariance

    PD = 1.  # probability of detection
    PG = .999  # probability of gating

    prediction_errors = []
    gospas = []

    for _ in tqdm(range(MC_ITERATIONS)):
        r = RadarCustomTwoTargets(curve_type, common_offset=100, mirror_offset=mirror_offset, R=R, lambd=lambd,
                                  debug=False)

        radar_stream = r.get_measurements(clutter=True)
        _ = next(radar_stream)
        radar_x0 = r.x[0]

        filt = JPDA_class(F, H, Q, R, PD=PD, PG=PG, lambd=r.lambd, x_0=radar_x0.T)

        for k in range(r.get_trajectory_length() - 1):
            z_all = next(radar_stream)  # get measurements

            filt.predict()
            rmse = np.sqrt(np.square(np.subtract(filt.x, r.x[k].T)).mean())
            prediction_errors.append(rmse)

            filt.update(z_all)
            filt.log(k)

        # calculate GOSPA for this run
        gospa = calculate_gospa(filt.x, r.x[-1].T)
        gospas.append(gospa)

    # return mean RMSE and GOSPA
    return np.mean(prediction_errors), np.std(prediction_errors), np.mean(gospas), np.std(gospas)


def run_experiment(out_path, curve_type, mirror_offset, lambd, R, JPDA_class):
    np.random.seed(SEED)

    F = np.array([[1, 1, 0, 0],  # state transition matrix
                  [0, 1, 0, 0],
                  [0, 0, 1, 1],
                  [0, 0, 0, 1]])

    H = np.array([[1., 0, 0, 0],  # measurement matrix
                  [0., 0, 1, 0]])
    Q = np.diag([.1, .1, .01, .01])  # process noise covariance
    R = R * np.eye(2)  # measurement noise covariance

    PD = 1.  # probability of detection
    PG = .999  # probability of gating

    x_targets_log = []
    prediction_error = []
    frames_path = out_path / "frames"
    frames_path.mkdir(parents=True, exist_ok=True)

    r = RadarCustomTwoTargets(curve_type, common_offset=100, mirror_offset=mirror_offset, R=R, lambd=lambd,
                              debug=False)

    radar_stream = r.get_measurements(clutter=True)
    _ = next(radar_stream)
    radar_x0 = r.x[0]

    filt = JPDA_class(F, H, Q, R, PD=PD, PG=PG, lambd=r.lambd, x_0=radar_x0.T)

    for k in range(r.get_trajectory_length() - 1):
        z_all = next(radar_stream)  # get measurements
        x_targets_log.append(r.x[-1].copy())

        filt.predict()

        rmse = np.sqrt(np.square(np.subtract(filt.x, r.x[k].T)).mean())
        prediction_error.append(rmse)

        filt.update(z_all)
        filt.log(k)

    # accumulate and dump statistics to file
    np.save(out_path / "x_targets_log.npy", np.array(x_targets_log))
    np.save(out_path / "prediction_error.npy", np.array(prediction_error))
    filt.dump_log(out_path)

    # plot prediction error
    plt.plot(prediction_error)
    plt.xlabel("Time")
    plt.ylabel("Prediction error")
    plt.title("Prediction error")
    plt.savefig(out_path / "prediction_error.png", bbox_inches='tight', dpi=100)
    sleep(0.1)
    plt.close('all')


def run_param_grid_experiment(model_mapping):
    for model_type in ['baseline', 'pruning', ]:
        experiment_root = pathlib.Path("experiment_results/param_grid")/ "240427" / model_type
        experiment_root.mkdir(parents=True, exist_ok=True)

        model_class = model_mapping[model_type]
        for curve_type in tqdm(['angle', 'curve', 'line', 'trapezoid'], desc=f"Curve type", position=0):
            curve_type_path = experiment_root / curve_type
            for mirror_offset in tqdm([1, 2, 5, 10, 20], desc=f"Mirror offset", position=1):
                for lambd in tqdm([.0001, .0005, .001, .003], desc=f"Lambd", position=2):
                    for R in tqdm([.5, 1., 5., 10.], desc=f"R", position=3):
                        experiment_path = curve_type_path / f"mirror_offset_{mirror_offset}_lambd_{lambd}_R_{R}"
                        experiment_path.mkdir(parents=True, exist_ok=True)
                        if (experiment_path / "z_log.npy").exists():
                            continue
                        run_experiment(experiment_path, curve_type, mirror_offset, lambd, R, model_class)
                        gc.collect()


def run_montecarlo(model_mapping):
    results = []
    curve_type = 'trapezoid'
    mirror_offset = 2
    # lambd = .0015
    R = 5.
    
    for model_type in ['pruning', 'baseline',]:
        experiment_root = pathlib.Path("experiment_results/experiments_MC") / f"240427"
        experiment_root.mkdir(parents=True, exist_ok=True)

        model_class = models_mapping[model_type]
        for lambd in [.0005, .001, .003, 0.005]:
            metrics = run_montecarlo_experiment(curve_type, mirror_offset, lambd, R, model_class)
            results.append((model_type, curve_type, mirror_offset, R, lambd, *metrics))
            gc.collect()
    # dump results
    np.save(experiment_root / "baseline_pruning_results.npy", np.array(results))


if __name__ == "__main__":
    models_mapping = {
        'baseline': JPDA,
        'pruning': JPDA_star,
    }
    # run_montecarlo(models_mapping)
    run_param_grid_experiment(models_mapping)
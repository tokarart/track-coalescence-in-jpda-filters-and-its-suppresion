import warnings

import numpy as np
from scipy.stats import multivariate_normal as mvn
from scipy.stats import poisson, uniform


class Radar(object):
    def __init__(self, A, H, Q, R, PD,
                 x, z,
                 lambd, area_vol, ndat, clutter_start_step=20):

        self.x = x.copy()
        self.z = z.copy()

        self.clutter_start_step = clutter_start_step

        self.ndat = ndat

        self.A = A
        self.H = H
        self.Q = Q
        self.R = R

        self.PD = PD
        self.lambd = lambd
        self.area_vol = area_vol

    def generate_measurement(self, t):
        # Generate trajectory states and measurements
        self.x[t] = self.A @ self.x[t - 1] + mvn.rvs(cov=self.Q)  # Simulation of SSM: new state
        self.z[t] = self.H @ self.x[t] + mvn.rvs(cov=self.R)  # Simulation of SSM: new measurement

        return self.x[t], self.z[t]

    def generate_measurements_with_clutter(self, t):
        z_clutter = None
        # Generate clutter
        if t < self.clutter_start_step:
            z_all = np.atleast_2d(self.z[t])
        else:
            nclutter = poisson.rvs(self.area_vol ** 2 * self.lambd)  # number of clutter points
            z_clutter = uniform.rvs(self.z[t] - self.area_vol / 2, self.area_vol, size=(nclutter, 2))

            # Target detection based on P_D
            if uniform.rvs() < self.PD:
                z_all = np.r_[np.atleast_2d(self.z[t]), z_clutter]
            else:
                z_all = z_clutter

        return z_clutter, z_all


class RadarMultiple(object):
    def __init__(self, A, H, Q, R, PD,
                 x, z,
                 lambd, area_vol, ndat, ntarget,
                 xmin, xmax, ymin, ymax,
                 clutter_start_step=20):

        self.x = x.copy()
        self.z = z.copy()

        self.xmin, self.xmax = xmin, xmax
        self.ymin, self.ymax = ymin, ymax

        self.clutter_start_step = clutter_start_step

        self.ndat = ndat
        self.ntarget = ntarget

        self.A = A
        self.H = H
        self.Q = Q
        self.R = R

        self.PD = PD
        self.lambd = lambd
        self.area_vol = area_vol

    def generate_measurment(self, i, k):
        # Generate trajectory states and measurements
        self.x[i, k] = self.A @ self.x[i, k - 1] + mvn.rvs(cov=self.Q)  # Simulation of SSM: new state
        self.z[i, k] = self.H @ self.x[i, k] + mvn.rvs(cov=self.R)  # Simulation of SSM: new measurement

        return self.x[i, k], self.z[i, k]

    def generate_measurments(self, k):
        for i in range(self.ntarget):
            self.x[i, k], self.z[i, k] = self.generate_measurment(i, k)

        return self.x[i, k], self.z[i, k]

    def generate_measurments_w_clutter(self, k):
        z_clutter = None
        # Generate clutter
        if k < self.clutter_start_step:
            z_all = np.atleast_2d(self.z[:, k])
        else:
            nclutter = poisson.rvs(self.area_vol ** 2 * self.lambd)  # number of clutter points
            # z_clutter = uniform.rvs(self.z[k]-self.area_vol/2, self.area_vol, size=(nclutter, 2))
            center = np.array([(self.xmax + self.xmin) / 2, (self.ymax + self.ymin) / 2])
            z_clutter = uniform.rvs(center - self.area_vol / 2, self.area_vol, size=(nclutter, 2))
            # Target detection based on P_D
            if uniform.rvs() < self.PD:
                z_all = np.r_[np.atleast_2d(self.z[:, k]), z_clutter]
            else:
                z_all = z_clutter

        return z_clutter, z_all


class RadarCustomTwoTargets:
    def __init__(self,
                 traj_type='curve',
                 common_offset=100,
                 mirror_offset=1,
                 R=1.8,
                 lambd=0,
                 debug=False
                 ):
        if traj_type not in ['angle', 'curve', 'line', 'trapezoid']:
            # warning
            warnings.warn(
                'traj_type should be one of "angle", "curve", "line", "trapezoid". Set to "curve" by default.')
            traj_type = 'curve'
        self.traj_type = traj_type
        self.traj = None
        self.common_offset = common_offset
        self.mirror_offset = mirror_offset
        self.debug = debug
        self.R = R * np.eye(2)  # covariance matrix for measurement uncertainty
        self.noise = mvn(mean=[0, 0], cov=self.R)
        self.area_side_len = None
        self.lambd = lambd
        self._xdim = 4
        self._zdim = 2
        self.PD = 1.
        self.detected = np.array([True, True])

        self.x = []
        self.z = []

        self._read_traj()

    def _read_traj(self):
        self.traj = np.load(f'data/traj_{self.traj_type}.npy')
        # apply mirror offset
        self.traj[0, :] = self.traj[0, :] - self.mirror_offset
        # calculate sidelenf from min max and some margin
        self.area_side_len = np.max(self.traj) - np.min(self.traj) + 10
        if self.debug:
            print(f'Read trajectory from data/traj_{self.traj_type}.npy '
                  f'of {self.traj.shape[1]} points. '
                  f'Applied mirror offset of {self.mirror_offset} to x.')

    def get_trajectory_length(self):
        return self.traj.shape[1]

    def get_traj_bounding_box(self):
        const = 20
        x_min = np.min(self.traj[0, :]) - const + self.common_offset
        x_max = np.max(self.traj[0, :]) + const + self.common_offset * 2
        y_min = np.min(self.traj[1, :]) - const
        y_max = np.max(self.traj[1, :]) + const
        return x_min, x_max, y_min, y_max

    def get_measurements(self, clutter=False):
        prev_first_target = None
        prev_second_target = None
        # generator
        for i in range(self.traj.shape[1]):
            first_target = self.traj[:, i].copy()
            first_target[0] = first_target[0] + self.common_offset
            second_target = self.traj[:, i].copy()
            # flip x axis for second target
            second_target[0] = -second_target[0]
            second_target[0] = second_target[0] + self.common_offset

            # add velocity to state vector
            if i == 0:  # first point
                first_target_v = np.zeros(2)
                second_target_v = np.zeros(2)
            else:
                first_target_v = first_target - prev_first_target
                second_target_v = second_target - prev_second_target

            # update prev values
            prev_first_target = first_target
            prev_second_target = second_target

            # add noise
            first_target += self.noise.rvs()
            second_target += self.noise.rvs()

            # construct states
            first_state = np.hstack((first_target[0], first_target_v[0], first_target[1], first_target_v[1]))
            second_state = np.hstack((second_target[0], second_target_v[0], second_target[1], second_target_v[1]))
            x_target = np.array([first_state, second_state])
            # x_target = np.array([first_state, second_state]).reshape(2, self._xdim, 1)
            self.x.append(x_target)

            z_target = np.array([first_target, second_target])
            self.z.append(z_target)
            # self.z.append(z_target.reshape(2, self._zdim, 1))

            u = uniform.rvs(size=2)
            self.detected = u <= self.PD
            z_target = z_target[self.detected]
            z_all = z_target

            z_clutter = np.array([], dtype=np.float64).reshape(0, self._zdim)
            if clutter:
                nclutter_pts = poisson.rvs(self.area_side_len ** 2 * self.lambd)
                z_clutter = uniform.rvs(self.common_offset - self.area_side_len / 2,
                                        scale=self.area_side_len,
                                        size=(nclutter_pts, 2))
                # subtract common offset from y axis
                z_clutter[:, 1] = z_clutter[:, 1] - self.common_offset

                z_all = np.r_[z_target, z_clutter]

            yield z_all

import numpy as np

class KF:
    def __init__(self, F, B, H, Q, R, P_multiplier=1000.) -> None:
        self.F = F
        self.B = B
        self.H = H
        self.Q = Q
        self.R = R
        self.P = np.eye(F.shape[0]) * P_multiplier  # initial uncertainty
        self.x = np.zeros(F.shape[0])
        self.n = F.shape[0]
        self.log_x = []

    def predict(self, u=None):
        x_minus = self.F @ self.x
        if u is not None:
            x_minus += self.B @ u
        P_minus = self.F @ self.P @ self.F.T + self.Q
        self.x = x_minus
        self.P = P_minus

    def update(self, z):
        innovation = z - self.H @ self.x  # y
        innovation_cov = self.H @ self.P @ self.H.T + self.R
        K_gain = self.P @ self.H.T @ np.linalg.inv(innovation_cov)
        x_plus = self.x + K_gain @ innovation
        P_plus = self.P - K_gain @ self.H @ self.P
        self.x = x_plus
        self.P = P_plus

    def log(self):
        self.log_x.append(self.x.copy())

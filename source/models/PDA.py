import numpy as np
from scipy.stats import multivariate_normal as mvn
from src.models.utils import mahalanobis


class PDA:
    def __init__(self, A, B, H, Q, R,
                 x_0,
                 clutter_rate, gate_probability, detection_probability, validation_region_size,
                 area_side_length, n_data_points,
                 P_multiplier=1000.) -> None:
        self.A = A  # state transition matrix
        self.B = B  # control matrix
        self.H = H  # observation matrix
        self.Q = Q  # process noise covariance
        self.R = R  # observation noise covariance

        self.n_data_points = n_data_points

        #  state related data structures
        self.state_len = A.shape[0]
        self.x = np.zeros((self.n_data_points, self.state_len))  # state vector
        self.x[0] = x_0
        self.P = np.zeros((self.n_data_points, self.state_len, self.state_len))  # state covariance matrix
        self.P[0] = np.eye(self.state_len) * P_multiplier  # initial uncertainty

        # measurement related data structures
        self.measurement_len = H.shape[0]
        self.z = np.zeros((self.n_data_points, self.measurement_len))  # measurement vector
        self.S = np.zeros(
            (self.n_data_points, self.measurement_len, self.measurement_len))  # measurement covariance matrix
        self.z_validated = None  # validated measurements

        # clutter related data structures
        self.lmbda = clutter_rate
        self.gamma = validation_region_size
        self.PD = detection_probability
        self.PG = gate_probability
        self.area_side_length = area_side_length

        # betas
        self.beta = None
        self.beta0 = None

    def predict_state(self, t, u=None):  # A: KF prediction (independent of measurements)
        self.x[t] = self.A @ self.x[t - 1]
        if u is not None:
            self.x[t] += self.B @ u
        self.P[t] = self.A @ self.P[t - 1] @ self.A.T + self.Q

    def predict_measurements(self, t):  # B: Measurement prediction
        self.z[t] = self.H @ self.x[t]
        self.S[t] = self.H @ self.P[t] @ self.H.T + self.R

    def measurement_validation(self, t, z_all):  # C: Measurement validation - Elipsoidal Gating
        mah_distance = mahalanobis(z_all, self.z[t], self.S[t])
        self.z_validated = z_all[mah_distance <= self.gamma]
        return self.z_validated

    def data_association_probs(self, t):  # D: Data association probabilities
        PD_likelihood = self.PD * mvn.pdf(self.z_validated, self.z[t], self.S[t])
        PD_likelihood = np.atleast_1d(PD_likelihood)  # make sure it's a 1D array
        norm_const = self.lmbda * (1 - self.PD * self.PG) + np.sum(PD_likelihood)
        self.beta = PD_likelihood / norm_const  # at least one measurement is valid
        self.beta0 = self.lmbda * (1 - self.PD * self.PG) / norm_const  # no measurement is valid

    def update(self, t):  # E: State update
        # 1. Kalman gain
        K = self.P[t] @ self.H.T @ np.linalg.inv(self.S[t])

        # 2. Innovations
        innovations = self.z_validated - self.H @ self.x[t]
        combined_innovations = self.beta @ innovations

        # 3. Kalman Update state
        self.x[t] += K @ combined_innovations

        # 4. Kalman Update state covariance
        P_with_valid_measurements = self.P[t] - K @ self.S[t] @ K.T
        P_without_valid_measurements = self.P[t].copy()
        S_sum = np.zeros_like(self.S[t])

        for j in range(self.beta.size):
            S_sum += self.beta[j] * np.outer(innovations[j], innovations[j])
        P_mix = K @ (S_sum - np.outer(combined_innovations, combined_innovations)) @ K.T
        self.P[t] = (1 - self.beta0) * P_with_valid_measurements + self.beta0 * P_without_valid_measurements + P_mix
from itertools import product
import numpy as np
from scipy.stats import chi2, multivariate_normal as mvn
from models.utils import mahalanobis

class JPDA:
    def __init__(self,
                 F,
                 H,
                 Q,
                 R,
                 PG=1.,
                 PD=1.,
                 lambd=0.,
                 x_0=None,
                 P_0=None, ):
        P_default = [[R[0, 0], 0, R[0, 0], 0],
                     [0, R[1, 1], 0, R[1, 1]],
                     [R[0, 0], 0, 2 * R[0, 0], 0],
                     [0, R[1, 1], 0, 2 * R[1, 1]]]

        self.F = F
        self.H = H
        self.Q = Q
        self.R = R
        self.lambd = lambd  # clutter rate
        self.PG = PG  # gate probability
        self.PD = PD  # detection probability
        self.gamma = chi2.ppf(self.PG, df=2)
        self.x = x_0
        self.x_prev = None
        self.n_targets = self.x.shape[1]
        self.n_dim = self.x.shape[0]
        self.P = np.array(P_default * self.n_targets).reshape((self.n_targets, self.n_dim,
                                                               self.n_dim)) if P_0 is None else P_0
        self.S = None
        self.z = None
        self.area_side_len = 0

        self.updated = np.zeros(shape=(self.n_targets, 1), dtype=bool)

        # logging
        self.time_log = []
        self.x_log = []
        self.P_log = []
        self.z_log = []
        self.S_log = []
        self.updated_indicator_log = []
        self.misdetections = 0

    def predict(self):
        # set updated to False
        self.updated = np.zeros(shape=(self.n_targets, 1), dtype=bool)
        self.x_prev = self.x.copy()

        # state prediction
        self.x = self.F @ self.x
        self.P = self.F @ self.P @ self.F.T + self.Q

        # measurement prediction
        self.z = self.H @ self.x
        self.S = self.H @ self.P @ self.H.T + self.R

    def update(self, z_all, bias_removal=True):
        # perform elipsoidal gating
        z_valid, z_valid_idx = self._perform_gating(z_all)

        # construct cost matrix
        C = self._construct_cost_matrix(z_all, z_valid, z_valid_idx)
        A, all_hypotheses = self._construct_A_matrices(z_all, z_valid_idx)
        assoc_probs = self._calculate_association_probabilities(all_hypotheses, C, A)

        # PDA update
        betas0, betas = self._calculate_betas(all_hypotheses, assoc_probs)
        for c_target in range(self.n_targets):
            if z_valid[c_target].size == 0:
                # missed detection
                self.misdetections += 1
                continue
            self._update_target(z_valid[c_target], betas0[c_target], betas[c_target], c_target)

        # # Indicator
        # self.updated = 1

    def _update_target(self, z_valid, beta0, beta, target_idx):
        P = self.P[target_idx]
        z = self.z[:, target_idx]
        S = self.S[target_idx]
        x = self.x[:, target_idx]

        W = P @ self.H.T @ np.linalg.inv(S)

        # E.2) Innovations
        innovations = z_valid - z
        combined_innovation = beta @ innovations

        # E.3) KF update of state
        x = x + W @ combined_innovation

        # E.4) KF update of covariance
        P_measurements = P - W @ S @ W.T
        P_no_measurement = P.copy()
        suma = np.zeros_like(S)
        for j in range(beta.size):
            suma += beta[j] * np.outer(innovations[j], innovations[j])
        P_mix = W @ (suma - np.outer(combined_innovation, combined_innovation)) @ W.T
        P = (1 - beta0) * P_measurements + beta0 * P_no_measurement + P_mix

        self.x[:, target_idx] = x
        self.P[target_idx] = P
        self.updated[target_idx] = 1

    def _perform_gating(self, z_all):
        z_valid = []
        z_valid_idx = []
        for i in range(self.n_targets):
            distances = mahalanobis(z_all, self.z[:, i], self.S[i])
            z_valid_indices = np.where(distances <= self.gamma)[0]
            z_valid.append(z_all[z_valid_indices])
            z_valid_idx.append(z_valid_indices)

        return z_valid, z_valid_idx

    def _construct_cost_matrix(self, z_all, z_valid, z_valid_idx):
        C_1 = np.zeros(shape=(self.n_targets, len(z_all)))
        C_2 = np.diag([self.lambd * (1 - self.PD * self.PG)] * self.n_targets)
        for c_target in range(self.n_targets):
            z_target = z_valid[c_target]
            z_target_idx = z_valid_idx[c_target]

            for h in range(len(z_target_idx)):
                detection_likelihood = self.PD * mvn.pdf(z_target[h], self.z[:, c_target], self.S[c_target])
                C_1[c_target, z_target_idx[h]] = detection_likelihood

        C = np.c_[C_1, C_2]
        return C

    @staticmethod
    def has_duplicates(data):
        """
        Checks if a list contains duplicates, excluding 0.

        Args:
          data: A list of numbers.

        Returns:
          True if the list contains duplicates (excluding 0), False otherwise.
        """
        seen = set()
        for num in data:
            if num != -1 and num in seen:
                return True
            seen.add(num)
        return False

    def _construct_A_matrices(self, z_all, z_valid_idx):
        """Construct all possible hypotheses matrices"""
        A = []
        # add zero hypothesis to every set
        z_val = z_valid_idx.copy()
        z_val_all = []

        for hypothesis in z_val:  # add zero hypothesis to every set
            z_val_all.append(np.insert(hypothesis, 0, -1))

        n_measurements = len(z_all)
        all_hypotheses = list(product(*z_val_all))
        for z in all_hypotheses:
            if self.has_duplicates(z):  # remove not valid
                # remove hypothesis
                all_hypotheses.remove(z)
            A_curr = np.zeros(shape=(self.n_targets, n_measurements + self.n_targets))
            for i in range(self.n_targets):
                if z[i] == -1:  # missed detections
                    A_curr[i, n_measurements + i] = 1
                else:
                    A_curr[i, z[i]] = 1
            A.append(A_curr)
        return A, all_hypotheses

    def _calculate_association_probabilities(self, all_hypotheses, C, A):
        """Calculate association probabilities"""
        assoc_probs = []
        products = np.array(A) @ C.T
        for i, h in enumerate(all_hypotheses):
            product = products[i]
            assoc_prob_i = np.sum(np.diagonal(product))  # Tr(A.T @ C)
            assoc_probs.append(assoc_prob_i)
        # normalize
        assoc_probs = np.array(assoc_probs) / np.sum(assoc_probs)
        return assoc_probs

    def _calculate_betas(self, all_hypotheses, assoc_probs):
        beta_0 = np.array(self._calculate_beta_0(all_hypotheses, assoc_probs))
        beta = self._calculate_beta(all_hypotheses, assoc_probs)
        return beta_0, beta

    def _calculate_beta_0(self, all_hypotheses, assoc_probs):
        beta_0 = []
        for t in range(self.n_targets):
            # find all hypothesis where target t is not detected == -1
            zero_hypotheses_idx = [i for i, h in enumerate(all_hypotheses) if h[t] == -1]
            beta_0_t = np.sum([assoc_probs[i] for i in zero_hypotheses_idx])
            beta_0.append(beta_0_t)
        return beta_0

    def _calculate_beta(self, all_hypotheses, assoc_probs):
        beta = []
        for c_target in range(self.n_targets):
            beta_c = []
            c_target_possible_assoc = sorted(
                set([h[c_target] for i, h in enumerate(all_hypotheses) if h[c_target] != -1]))
            for assoc in c_target_possible_assoc:
                assoc_hypotheses_idx = [i for i, h in enumerate(all_hypotheses) if h[c_target] == assoc]
                beta_c_t = np.sum([assoc_probs[i] for i in assoc_hypotheses_idx])
                beta_c.append(beta_c_t)
            beta.append(np.array(beta_c))
        return beta

    def log(self, timestamp):
        self.time_log.append(timestamp)
        self.x_log.append(self.x.copy())
        self.P_log.append(self.P.copy())
        self.z_log.append(self.z.copy())
        self.S_log.append(self.S.copy())
        self.updated_indicator_log.append(self.updated)

    def dump_log(self, out_path):
        np.save(out_path / "x_log.npy", np.array(self.x_log))
        np.save(out_path / "P_log.npy", np.array(self.P_log))
        np.save(out_path / "z_log.npy", np.array(self.z_log))
        np.save(out_path / "S_log.npy", np.array(self.S_log))
        np.save(out_path / "updated_indicator_log.npy", np.array(self.updated_indicator_log))
        np.save(out_path / "misdetections.npy", np.array(self.misdetections))

import matplotlib.transforms as transforms
import numpy as np
from matplotlib.patches import Ellipse
import gospapy


def confidence_ellipse(loc, cov, ax, n_std=3.0, facecolor='none', rot=45, **kwargs):
    pearson = cov[0, 1] / np.sqrt(cov[0, 0] * cov[1, 1])
    # Using a special case to obtain the eigenvalues of this
    # two-dimensional dataset.
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0), width=ell_radius_x * 2, height=ell_radius_y * 2,
                      facecolor=facecolor, **kwargs)

    # Calculating the standard deviation of x from
    # the squareroot of the variance and multiplying
    # with the given number of standard deviations.
    scale_x = np.sqrt(cov[0, 0]) * n_std
    mean_x = loc[0]

    # calculating the standard deviation of y ...
    scale_y = np.sqrt(cov[1, 1]) * n_std
    mean_y = loc[1]

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)


def mahalanobis(d, mean, Sigma):
    Sigma_inv = np.linalg.inv(Sigma)
    xdiff = np.atleast_2d(d - mean.T)
    return np.einsum('ij,im,mj->i', xdiff, xdiff, Sigma_inv)


def calculate_gospa(tracks, target, c=3, p=1, full_output=False):
    gospa, assignment, loc_err, miss_err, false_err = gospapy.calculate_gospa(
        tracks, target, c=c, p=p)
    if full_output:
        return gospa, assignment, loc_err, miss_err, false_err
    return gospa


def has_duplicates(data):
    """
    Checks if a list contains duplicates, excluding 0.

    Args:
      data: A list of numbers.

    Returns:
      True if the list contains duplicates (excluding 0), False otherwise.
    """
    seen = set()
    for num in data:
        if num != -1 and num in seen:
            return True
        seen.add(num)
    return False

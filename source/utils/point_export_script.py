import json 

# Put this script in the Blender's text editor and run it with the object selected

def getPoints(obj):
    if obj.type == "CURVE":
        splines = obj.data.splines
        for spline in splines:
            if spline.type == "NURBS":
                points = []
                for point in spline.points:
                    points.append(point.co)
                    points = [[p.x, p.y] for p in points] 
                    return points


export_path = "/Users/saladartem/Documents/FIT/Masters/Thesis/src/data/trapezoid_points.json"
ob = C.selected_objects[0]
points = getPoints(ob)
with open("thesis_tokarart/source/data.json", "w") as fd: 
    json.dump(points, fd)
